# Linguistic Universe

The idea of this file is to outline the linguistic world in which Fisterfilms is located. A "corporate language" or "corporate linguistic identity" if you will, an aid in writing texts for this thing.

This document is by no means to remain static. As language lives, this document shall change over time and adopt to our needs.

Eventually, German and English should be treated in here. We'll probably have a lot of German and English texts, that's why. Texts of each language must not be translated word by word, not even convey the same exact meaning, besides of facts and data, but rather feel the same. Not everything needs to be translated in both languages, we decide case by case which languages are appropriate for the audience. Not translating everything is to reduce the amount of work required.

## Todo
 * Describe the "feeling" (corporate feeling!) using examples. Also, the word list should help.
 * Fill wordlists.
 * Suggest workarounds for items on the negative wordlist, either inline or by adding links to entries in the positive wordlist.

## Feeling

Generally when it feels like porn language it's good, i.e. filthy or "schmuddelig".



## Wordlist
 * *handmade* we have a hipster logo, better than "artisan". Though I like the idea of bringing that hipster/artisan aspect into the identity, i.e. "handmade hard core \[porn\]" (this has so many levels in the context of Fisterfilms).
 * *hard core* our hipster logo actually stems culturally from the famous [New York HardXCore Logo](https://daily.redbullmusicacademy.com/2013/06/iconic-logos#newyorkhardcore) Also see: [yourlogoisnothardcore](https://yourlogoisnothardcore.tumblr.com/). Thus, when we say we make hard core \[porn\], there's the conection. This also makes a good fit into organizing underground punk/metal/grindcore events, the fanzine etc.

## Negative Wordlist (AVOID!)
 * *making love* (LOL) depends on the context, just putting here something to start the list.
  * *vanilla* while a good fit from its cultural usage e.g. in the BDSM scene, its mainly used to exclude people, to describe "the others", the uninitiated. We want to be inclusive, open etc. We will scare people away anyways, without beeing mean to them, no need to draw borders.